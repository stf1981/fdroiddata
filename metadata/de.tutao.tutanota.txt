AntiFeatures:NonFreeNet
Categories:Internet
License:GPL-3.0-or-later
Web Site:https://tutanota.com
Source Code:https://github.com/tutao/tutanota
Issue Tracker:https://github.com/tutao/tutanota/issues
Donate:https://tutanota.com/community#donate
Bitcoin:3MDrR5gaMvL8sphuQLX6BvPPKYNArdXsv6

Auto Name:Tutanota

Repo Type:git
Repo:https://github.com/tutao/tutanota.git

Build:3.35.13,335130
    commit=tutanota-android-release-3.35.13
    subdir=app-android/app
    sudo=curl -Lo node.tar.xz https://nodejs.org/dist/v8.11.3/node-v8.11.3-linux-x64.tar.xz && \
        echo "08e2fcfea66746bd966ea3a89f26851f1238d96f86c33eaf6274f67fce58421a node.tar.xz" | sha256sum -c - && \
        tar xJf node.tar.xz && \
        cp -a node-v8.11.3-linux-x64/. /usr/local/
    gradle=yes
    prebuild=cd ../.. && \
        rm -rf app-ios && \
        sed -i '/flow-bin/d' package.json && \
        npm install && \
        node dist prod

Auto Update Mode:Version tutanota-android-release-%v
Update Check Mode:Tags tutanota-android-release-*
Current Version:3.35.13
Current Version Code:335130
