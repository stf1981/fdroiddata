Categories:
  - Internet
License: GPL-3.0-only
AuthorName: Christian Schneppe
SourceCode: https://github.com/kriztan/Pix-Art-Messenger
IssueTracker: https://github.com/kriztan/Pix-Art-Messenger/issues
Changelog: https://github.com/kriztan/Pix-Art-Messenger/blob/HEAD/CHANGELOG.md

AutoName: Pix-Art Messenger
Summary: A Jabber/XMPP chat client
Description: |-
    Pix-Art Messenger is a fork of [[eu.siacs.conversations]] with some changes,
    to improve usability.

    Connection security and the protection of personal data are very important
    for us, so we enforce encrypted connections between messenger and server,
    making it almost impossible to intercept and read your messages by strangers.
    If that's not enough for you, end-to-end message encryption such as OMEMO,
    OTR or OpenPGP can be used. This allows messages to be encrypted on the
    sending device and decrypted only by the receiving device intended for
    reception.
    Since version 1.20.0, a hint is displayed directly in the chat, as long as
    you write unencrypted and encryption is possible.

    Features:
    * End-to-end encryption with either [http://conversations.im/omemo/ OMEMO], [https://otr.cypherpunks.ca/ OTR] or [http://openpgp.org/about/ OpenPGP]
    * Send and receive images as well as other files
    * Send and receive locations
    * Send and receive voice messages
    * Integration of profile pictures (avatars) of your contacts
    * Synchronize message history with other clients
    * Conferences or group chats
    * Address book integration (there is no exchange of your address book with the server)
    * Very low battery consumption
    * Status messages
    * Daily backup of database to local storage

RepoType: git
Repo: https://github.com/kriztan/Pix-Art-Messenger

Builds:
  - versionName: 2.0.2
    versionCode: 233
    commit: 2.0.2
    gradle:
      - standard

  - versionName: 2.1.0 (beta 2018-07-15)
    versionCode: 237
    commit: 2.1.0.3
    gradle:
      - standard

  - versionName: 2.1.0
    versionCode: 238
    commit: 2.1.0
    gradle:
      - standard

AutoUpdateMode: Version %v
UpdateCheckMode: Tags ^[0-9.]+$
CurrentVersion: 2.1.0
CurrentVersionCode: 238
